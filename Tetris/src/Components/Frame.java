package Components;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Frame extends JFrame {
    public Frame(String name) throws HeadlessException, InterruptedException {
        setLayout(new GridLayout(1, 2));
        this.setTitle(name);
        this.setLocationRelativeTo(null);
        this.setSize(319, 339);
        this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
        GamePanel gamePanel = new GamePanel();
        StatusBar statusBar = new StatusBar();
        addKeyListener(new KeyListener() {

            public void keyPressed(KeyEvent e) {
                setFocusable(true);
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_LEFT:
                        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).moveLeft();
                        break;
                    case KeyEvent.VK_RIGHT:
                        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).moveRight();
                        break;
                    case KeyEvent.VK_DOWN:
                        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).moveDown();
                        break;
                    case KeyEvent.VK_UP:
                        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).swap();
                        break;
                }

            }

            public void keyReleased(KeyEvent e) {

            }

            public void keyTyped(KeyEvent e) {

            }

        });

        gamePanel.setBackground(Color.BLACK);
        statusBar.setBackground(Color.WHITE);
        this.add(gamePanel);
        this.add(statusBar);
        this.setVisible(true);

    }
}
