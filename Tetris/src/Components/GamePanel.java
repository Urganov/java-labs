package Components;

import Figures.*;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Random;

public class GamePanel extends JPanel implements Runnable {
    public static final Object syn = new Object();
    public ArrayList<Figure> allFigure;
    public int[][] gameField = new int[11][21];
    boolean start = false;

    GamePanel() {
        final int WIDTH = 10 * 15;    //10 blocks wide (10 * 15 = 150 )
        final int HEIGHT = 20 * 15;   //20 blocks high (20 * 15 = 300 )
        setSize(WIDTH, HEIGHT);
        for (int i = 0; i < 21; i++) {
            gameField[10][i] = 1;
        }
        for (int i = 0; i < 11; i++) {
            gameField[i][20] = 1;
        }
        allFigure = new ArrayList<>();
        new Thread(this).start();
    }


    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        for (int i = 0; i < allFigure.size(); i++) {
            Figure anAllFigure = allFigure.get(i);
            ArrayList<Rectangle2D.Float> figure = anAllFigure.figure;
            for (int i1 = 0; i1 < figure.size(); i1++) {
                Rectangle2D rectangle2D = figure.get(i1);
                g2.setColor(Color.BLUE);
                g2.fill(rectangle2D);
                g2.setColor(Color.GREEN);
                g2.draw(rectangle2D);
            }
        }
    }

    @Override
    public void run() {
        boolean gameOver = false;
        synchronized (syn) {
            while (!gameOver) {
                Random rnd = new Random(System.currentTimeMillis());
                int nextFigure = rnd.nextInt(7);
                switch (nextFigure) {
                    case 0:
                        allFigure.add(new Square(this));
                        break;
                    case 1:
                        allFigure.add(new Stick(this));
                        break;
                    case 2:
                        allFigure.add(new RightHatchet(this));
                        break;
                    case 3:
                        allFigure.add(new LeftHatchet(this));
                        break;
                    case 4:
                        allFigure.add(new LeftSwan(this));
                        break;
                    case 5:
                        allFigure.add(new RightSwan(this));
                        break;
                    case 6:
                        allFigure.add(new Stand(this));
                        break;
                }
                try {
                    syn.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                checkLine();
                gameOver = endGame();
            }
        }
    }

    private boolean endGame() {
        for (int i = 0; i < 10; i++) {
            if (gameField[i][0] == 1) {
                allFigure.clear();
                repaint();
                return true;
            }
        }
        return false;
    }

    private void checkLine() {
        for (int i = 0; i < 20; i++) {
            boolean wholeLine = true;
            for (int j = 0; j < 10; j++) {
                if (gameField[j][i] == 0) {
                    wholeLine = false;
                    break;
                }
            }
            if (wholeLine) {
                deletteLine(i);
                i--;
            }
        }

    }

    private void deletteLine(int i) {
        for (int j = i; j > 1; j--) {
            for (int k = 0; k < 10; k++) {
                gameField[k][j] = gameField[k][j - 1];
            }
        }
        for (int k = 0; k < 10; k++) {
            gameField[k][0] = 0;
        }
        for (Figure anAllFigure : allFigure) {
            for (int j = 0; j < anAllFigure.figure.size(); j++) {
                if (anAllFigure.figure.get(j).getY() == i * 15) {
                    anAllFigure.figure.remove(j);
                    j--;
                    continue;
                }
                if (anAllFigure.figure.get(j).getY() < i * 15) {
                    anAllFigure.figure.get(j).setRect(anAllFigure.figure.get(j).getX(), anAllFigure.figure.get(j).getY() + 15, 15, 15);
                }

            }
        }
        repaint();
    }

}
