package Figures;

class Hatchet extends Figure {

    public void swap() {
        int newX = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getX();
        int newY = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getY();
        if (newX - 15 < 0 || newX + 15 > 9 * 15) {
            return;
        }
        if (check(newX, newY)) {
            return;
        }
        switch (position % 4) {
            case 0:
                swapPosZ();
                break;
            case 1:
                swapPosOne();
                break;
            case 2:
                swapPosTwo();
                break;
            case 3:
                swapPosThree();
                break;
        }

        position++;
    }

    void swapPosThree() {

    }

    void swapPosTwo() {

    }

    void swapPosOne() {

    }

    void swapPosZ() {

    }

    private boolean check(int newX, int newY) {
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (gamePanel.gameField[newX / 15 + i][newY / 15 + j] == 1) {
                    return true;
                }
            }
        }
        return false;
    }
}
