package Figures;

public class Swan extends Figure {

    public void swap() {
        int newX = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(3).getX();
        int newY = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(3).getY();
        if (newX - 15 < 0) {
            return;
        }
        if (check(newX, newY)) {
            return;
        }
        switch (position % 2) {
            case 0:
                swapPosZ();
                break;
            case 1:
                swapPosOne();
                break;
        }

        position++;
    }

    void swapPosOne() {

    }

    void swapPosZ() {

    }

    private boolean check(int newX, int newY) {

        for (int i = 0; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (gamePanel.gameField[newX / 15 + i][newY / 15 + j] == 1) {
                    return true;
                }
            }
        }
        return false;
    }
}
