package Figures;

import Components.GamePanel;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class Square extends Figure {

    public Square(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
        position = 0;
        figure = new ArrayList<>(4);
        for (int i = 0; i < 4; i++) {
            Rectangle2D.Float rect = new Rectangle2D.Float();
            rect.setRect(i / 2 * 15, i % 2 * 15, 15, 15);
            figure.add(rect);
        }
        (new Thread(this)).start();
    }


}


