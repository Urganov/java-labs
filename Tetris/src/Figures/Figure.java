package Figures;

import Components.GamePanel;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class Figure implements Runnable {
    int position;
    public ArrayList<Rectangle2D.Float> figure;
    GamePanel gamePanel;
    private int speed = 500;

    public void moveRight() {
        for (Rectangle2D.Float aFigure : figure) {
            if (aFigure.x / 15 >= 9 || gamePanel.gameField[(int) aFigure.x / 15 + 1][(int) aFigure.y / 15 + 1] == 1) {
                return;
            }
            if (gamePanel.gameField[(int) aFigure.x / 15 + 1][(int) aFigure.y / 15] == 1) {
                return;
            }
        }
        for (Rectangle2D.Float aFigure : figure) {
            aFigure.x += 15;
        }
    }

    public void moveLeft() {
        for (Rectangle2D.Float aFigure : figure) {
            if (aFigure.x / 15 - 1 < 0 || gamePanel.gameField[(int) Math.floor(aFigure.x / 15 - 1)][(int) Math.floor(aFigure.y / 15 + 1)] == 1) {
                return;
            }
            if (gamePanel.gameField[(int) Math.floor(aFigure.x / 15 - 1)][(int) Math.floor(aFigure.y / 15)] == 1) {
                return;
            }

        }
        for (Rectangle2D.Float aFigure : figure) {
            aFigure.x -= 15;
        }
    }

    public void swap() {
    }

    public void moveDown() {
        speed = 3;
    }

    @Override
    public void run() {
        synchronized (GamePanel.syn) {
            speed = 50;
            boolean end = false;
            while (!end) {
                try {
                    Thread.sleep(speed);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (int j = 0; j < 4; j++) {
                    figure.get(j).y += 1;
                }
                gamePanel.repaint();
                for (int j = 0; j < 4; j++) {
                    double Y = figure.get(j).y / 15 + 1;
                    double X = figure.get(j).x / 15;
                    if (gamePanel.gameField[(int) X][(int) Math.floor(Y)] == 1) {
                        end = true;
                        break;
                    }
                }

            }
            for (int j = 0; j < 4; j++) {
                gamePanel.gameField[(int) figure.get(j).x / 15][(int) figure.get(j).y / 15] = 1;
            }
            GamePanel.syn.notify();
        }
    }
}
