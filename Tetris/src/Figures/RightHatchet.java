package Figures;

import Components.GamePanel;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class RightHatchet extends Hatchet {
    public RightHatchet(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
        position = 0;
        figure = new ArrayList<>(4);
        for (int i = 0; i < 3; i++) {
            Rectangle2D.Float rect = new Rectangle2D.Float();
            rect.setRect(0, i * 15, 15, 15);
            figure.add(rect);
        }
        Rectangle2D.Float rect = new Rectangle2D.Float();
        rect.setRect(15, 2 * 15, 15, 15);
        figure.add(rect);
        (new Thread(this)).start();
    }

    @Override
    void swapPosThree() {
        int newX = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getX();
        int newY = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getY();
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(0).setRect(newX, newY - 15, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(2).setRect(newX, newY + 15, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(3).setRect(newX + 15, newY + 15, 15, 15);

    }

    @Override
    void swapPosTwo() {
        int newX = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getX();
        int newY = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getY();
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(0).setRect(newX + 15, newY, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(2).setRect(newX - 15, newY, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(3).setRect(newX - 15, newY + 15, 15, 15);

    }

    @Override
    void swapPosOne() {
        int newX = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getX();
        int newY = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getY();
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(0).setRect(newX, newY + 15, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(2).setRect(newX, newY - 15, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(3).setRect(newX - 15, newY - 15, 15, 15);
    }

    @Override
    void swapPosZ() {
        int newX = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getX();
        int newY = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getY();
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(0).setRect(newX - 15, newY, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(2).setRect(newX + 15, newY, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(3).setRect(newX + 15, newY - 15, 15, 15);
    }
}





