package Figures;

import Components.GamePanel;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class LeftSwan extends Swan {
    public LeftSwan(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
        position = 0;
        figure = new ArrayList<>(4);
        for (int i = 0; i < 2; i++) {
            Rectangle2D.Float rect = new Rectangle2D.Float();
            rect.setRect(15 + i * 15, 0, 15, 15);
            figure.add(rect);
        }
        for (int i = 0; i < 2; i++) {
            Rectangle2D.Float rect = new Rectangle2D.Float();
            rect.setRect(i * 15, 15, 15, 15);
            figure.add(rect);
        }
        (new Thread(this)).start();
    }

    @Override
    void swapPosZ() {
        int newX = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(3).getX();
        int newY = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(3).getY();
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).setRect(newX - 15, newY - 15, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(0).setRect(newX - 15, newY, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(2).setRect(newX, newY + 15, 15, 15);
    }

    @Override
    void swapPosOne() {
        int newX = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(3).getX();
        int newY = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(3).getY();
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).setRect(newX + 15, newY - 15, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(0).setRect(newX, newY - 15, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(2).setRect(newX - 15, newY, 15, 15);
    }
}
