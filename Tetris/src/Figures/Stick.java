package Figures;

import Components.GamePanel;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class Stick extends Figure {
    public Stick(GamePanel gamePanel) {
        position = 0;
        this.gamePanel = gamePanel;
        figure = new ArrayList<>(4);
        for (int i = 0; i < 4; i++) {
            Rectangle2D.Float rect = new Rectangle2D.Float();
            rect.setRect(0, i * 15, 15, 15);
            figure.add(rect);
        }
        (new Thread(this)).start();
    }

    @Override
    public void swap() {
        int newX = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(0).getX();
        int newY = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(0).getY();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (position % 2 == 0 && newX + 3 * 15 > 9 * 15) {
                    return;
                }
                if (position % 2 == 1 && newY + 3 * 15 > 19 * 15) {
                    return;
                }
                if (gamePanel.gameField[newX / 15 + i][newY / 15 + j] == 1) {
                    return;
                }
            }
        }
        for (int i = 0; i < 4; i++) {
            Rectangle2D oneSquare = gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(i);
            if (position % 2 == 0) {
                if (i == 0) {
                    newY = (int) oneSquare.getY();
                    continue;
                }
                newX = (int) oneSquare.getX();
                oneSquare.setRect(newX + i * 15, newY, 15, 15);
            }
            if (position % 2 == 1) {
                if (i == 0) {
                    newX = (int) oneSquare.getX();
                    continue;
                }
                newY = (int) oneSquare.getY();
                oneSquare.setRect(newX, newY + i * 15, 15, 15);
            }
        }
        position++;
    }
}