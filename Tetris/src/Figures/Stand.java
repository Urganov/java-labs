package Figures;

import Components.GamePanel;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class Stand extends Figure {
    public Stand(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
        position = 0;
        figure = new ArrayList<>(4);
        for (int i = 0; i < 3; i++) {
            Rectangle2D.Float rect = new Rectangle2D.Float();
            rect.setRect(i * 15, 15, 15, 15);
            figure.add(rect);
        }
        Rectangle2D.Float rect = new Rectangle2D.Float();
        rect.setRect(15, 0, 15, 15);
        figure.add(rect);
        (new Thread(this)).start();
    }

    @Override
    public void swap() {
        int newX = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getX();
        int newY = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getY();
        if (newX - 15 < 0 || newX + 15 > 9 * 15) {
            return;
        }
        if (check(newX, newY)) {
            return;
        }
        switch (position % 4) {
            case 0:
                swapPosZ();
                break;
            case 1:
                swapPosOne();
                break;
            case 2:
                swapPosTwo();
                break;
            case 3:
                swapPosThree();
                break;
        }

        position++;
    }

    private void swapPosThree() {
        int newX = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getX();
        int newY = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getY();
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(0).setRect(newX - 15, newY, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(2).setRect(newX + 15, newY, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(3).setRect(newX, newY - 15, 15, 15);
    }

    private void swapPosTwo() {
        int newX = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getX();
        int newY = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getY();
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(0).setRect(newX, newY - 15, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(2).setRect(newX, newY + 15, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(3).setRect(newX + 15, newY, 15, 15);
    }

    private void swapPosOne() {
        int newX = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getX();
        int newY = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getY();
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(0).setRect(newX + 15, newY, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(2).setRect(newX - 15, newY, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(3).setRect(newX, newY + 15, 15, 15);
    }

    private void swapPosZ() {
        int newX = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getX();
        int newY = (int) gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(1).getY();
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(0).setRect(newX, newY + 15, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(2).setRect(newX, newY - 15, 15, 15);
        gamePanel.allFigure.get(gamePanel.allFigure.size() - 1).figure.get(3).setRect(newX - 15, newY, 15, 15);
    }

    private boolean check(int newX, int newY) {
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (gamePanel.gameField[newX / 15 + i][newY / 15 + j] == 1) {
                    return true;
                }
            }
        }
        return false;
    }
}
