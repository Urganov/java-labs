package ru.nsu.fit.yurganov;

public interface HttpHandler {
    HttpResponse handle(HttpRequest request);
}
