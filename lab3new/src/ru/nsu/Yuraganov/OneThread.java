package ru.nsu.Yuraganov;

import java.util.ArrayList;

public class OneThread implements Runnable {
    private boolean finish = false;
    private ArrayList<Runnable> tasks;
    private Thread thread;

    OneThread(ArrayList<Runnable> tasks) {
        thread = new Thread(this);
        thread.start();
        this.tasks = tasks;
    }

    @Override
    public void run() {
        while (!finish) {
            Runnable task;
            synchronized (ThreadPool.syn) {
                if (tasks.isEmpty()) {

                    try {
                        ThreadPool.syn.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                task = tasks.get(tasks.size() - 1);
                tasks.remove(tasks.size() - 1);
            }
            if (task != null) {
                task.run();
            }
        }
    }

    public void finish() {
        finish = true;
    }


    public void join() throws InterruptedException {
        thread.join();
    }
}
