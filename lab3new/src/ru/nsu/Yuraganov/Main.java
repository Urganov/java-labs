package ru.nsu.Yuraganov;

import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        ThreadPool testf = new ThreadPool(4);
        AtomicInteger i = new AtomicInteger(0);
        Runnable runnable = new Test(i);

        for (int j = 0; j < 1000; ++j) {
            testf.run(runnable);
        }
        testf.join();
        System.out.println(i.get());
    }
}

class Test implements Runnable {
    AtomicInteger i;

    Test(AtomicInteger i) {
        this.i = i;
    }

    @Override
    public void run() {
        for (int j = 0; j < 1000000; ++j) {
            i.incrementAndGet();
        }
    }
}