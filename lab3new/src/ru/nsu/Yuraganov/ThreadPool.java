package ru.nsu.Yuraganov;

import java.util.ArrayList;

public class ThreadPool {
    final static Object syn = new Object();
    private ArrayList<Runnable> tasks;
    private OneThread threads[];

    ThreadPool(int size) {
        tasks = new ArrayList<>();
        threads = new OneThread[size];
        for (int i = 0; i < size; i++) {
            OneThread thread = new OneThread(tasks);
            threads[i] = thread;
        }
    }

    public void join() throws InterruptedException {
        while (true) {
            synchronized (syn) {
                if (tasks.isEmpty()) {
                    break;
                }
            }
        }
        for (OneThread thread : threads) {
            thread.finish();
        }
        synchronized (syn) {
            syn.notifyAll();
        }
        for (OneThread thread : threads) {
            thread.join();
        }
    }

    public void run(Runnable task) {
        synchronized (syn) {
            tasks.add(task);
            syn.notify();
        }
    }
}
