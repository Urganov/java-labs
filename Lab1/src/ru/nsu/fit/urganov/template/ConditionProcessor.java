package ru.nsu.fit.urganov.template;

import java.util.Map;


public class ConditionProcessor implements TemplateProcessor {
    ConditionProcessor(String condition, String variable, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) {
        this.condition = condition;
        this.variable = variable;
        fillTemplate(rez, values, conditions, iterations);
    }

    private String condition;
    private String variable;
    private StringBuilder rez = new StringBuilder();

    @Override
    public String toString() {
        return rez.toString();
    }

    @Override
    public void fillTemplate(StringBuilder sb, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) {
        if (conditions.get(condition)) {
            rez.append(variable);
        }

    }
}
