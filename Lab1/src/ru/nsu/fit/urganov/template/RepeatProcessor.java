package ru.nsu.fit.urganov.template;

import java.util.Map;

public class RepeatProcessor implements TemplateProcessor {
    @Override
    public String toString() {
        return rez.toString();
    }

    RepeatProcessor(String condition, String variable, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) {
        this.condition = condition;
        this.variable = variable;
        fillTemplate(rez, values, conditions, iterations);
    }

    private String condition;
    private String variable;
    StringBuilder rez = new StringBuilder();

    @Override
    public void fillTemplate(StringBuilder sb, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) {
        int count = iterations.get(condition);
        for (int i = 0; i < count; i++) {
            rez.append(variable);
        }
    }
}
