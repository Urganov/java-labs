package ru.nsu.fit.urganov.template;

import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws IOException {
        try {
            Template f = new Template("<h1> Hello %name%!</h1><h2>%!if isUser!% Welcome back %!endif!% </h2>%!repeat jjj!% content %!endrepeat!%");
            Map<String, Integer> iterations = new HashMap<>();
            iterations.put("jjj", 2);
            Map<String, Boolean> conditions = new HashMap<>();
            conditions.put("isUser", true);
            Map<String, String> bbb = new HashMap<>();
            bbb.put("name", "Vlad");
            String fff = f.fill(bbb, conditions, iterations);
            int i = 0;
        } catch (NullPointerException err) {
            System.out.println("ArgumentNotFoundException");
        } catch (IllegalArgumentException err) {
            System.out.println("Bad input");
        } catch (InvalidInput invalidInput) {
            invalidInput.printStackTrace();
        }
    }
}
