package ru.nsu.fit.urganov.template;

import java.util.Map;

public class ValueProcessor implements TemplateProcessor {
    private StringBuilder rez = new StringBuilder();
    private String value;

    ValueProcessor(String value, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) {
        this.value = value;
        fillTemplate(rez, values, conditions, iterations);
    }

    @Override
    public String toString() {
        return rez.toString();
    }

    @Override
    public void fillTemplate(StringBuilder sb, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) {
        rez.append(values.get(value));
    }
}
