package ru.nsu.fit.urganov.template;

import java.util.Map;

public class StringProcessor implements TemplateProcessor {
    private StringBuilder rezString;

    StringProcessor(StringBuilder sb, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) {
        fillTemplate(sb, values, conditions, iterations);
    }

    @Override
    public String toString() {
        return rezString.toString();
    }

    @Override
    public void fillTemplate(StringBuilder sb, Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) {
        rezString = sb;

    }
}
