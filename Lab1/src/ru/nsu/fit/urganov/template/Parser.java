package ru.nsu.fit.urganov.template;

import java.lang.module.ModuleDescriptor;
import java.util.*;

public class Parser {
    private Integer i = 0;
    private Integer j = 0;

    private void findSubSting(String s, String startStatment, String endStatment, List<StringBuilder> parsedSring) throws InvalidInput {
        if (i < s.length() - endStatment.length() && s.substring(i, i + startStatment.length()).equals(startStatment)) {
            j = i;
            while (!s.substring(i, i + endStatment.length()).equals(endStatment)) {
                if (i >= s.length() - endStatment.length()) {
                    throw new InvalidInput();
                }
                i++;
            }
            //  i++;
            parsedSring.add(new StringBuilder(s.substring(j, i + endStatment.length())));
            i += endStatment.length();
            j = i;
        }
    }

    public List<StringBuilder> getParsedSring(String s) throws InvalidInput {
        String condition = "%!if ";
        String conditionEnd = "%!endif!%";
        String repeat = "%!repeat ";
        String repeatEnd = "%!endrepeat!%";
        List<StringBuilder> parsedSring = new ArrayList<StringBuilder>();
        i--;
        while (i < s.length() - 1) {
            i++;
            parsing(s, parsedSring);
            findSubSting(s, condition, conditionEnd, parsedSring);
            findSubSting(s, repeat, repeatEnd, parsedSring);
        }
        if (i > j) {
            j++;
            parsedSring.add(new StringBuilder(s.substring(j - 1, i)));
        }
        for (StringBuilder value : parsedSring) {
            if (value.charAt(value.length() - 1) == '\\') {
                value.deleteCharAt(value.length() - 1);
            }
        }
        return parsedSring;
    }

    private void parsing(String s, List<StringBuilder> parsedSring) throws InvalidInput {
        if (s.charAt(i) == '%' && i != 0) {
            parsedSring.add(new StringBuilder(s.substring(j, i)));
            j = i;
        }
        if (s.charAt(i) == '%' && s.charAt(i + 1) != '!') {
            j = i;
            i++;
            while (s.charAt(i) != '%') {
                i++;
                if (i >= s.length()) {
                    throw new InvalidInput();
                }
            }
            if (i.equals(j)) {
                throw new InvalidInput();
            }
            i++;
            parsedSring.add(new StringBuilder(s.substring(j, i)));
            j = i;
        }
    }


}

