package ru.nsu.fit.urganov.template;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class Template {
    final int NEXT_NEXT = 2;
    final int NEXT = 1;
    private String sb;

    Template(String s) {
        sb = s;
    }

    public String toString(List<TemplateProcessor> string) {
        StringBuilder finalString = new StringBuilder();
        for (TemplateProcessor i : string) {
            if (i.toString() != null) {
                finalString.append(i.toString());
            }
        }
        return finalString.toString();
    }

    String fill(Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws InvalidInput {
        Parser parser = new Parser();
        List<StringBuilder> parsedSring = parser.getParsedSring(sb);
        List<TemplateProcessor> finalRezault = new ArrayList<>();
        String condition = "%!if ";
        String endCondition = "%!endif";
        String repeat = "%!repeat ";
        String endRepeat = "%!endrepeat!%";
        int j = 0;
        for (StringBuilder i : parsedSring) {
            if (i == null) {
                continue;
            }
            if (i.charAt(0) != '%') {
                finalRezault.add(new StringProcessor(i, values, conditions, iterations));
                continue;
            }
            if (i.length() >= condition.length() && i.substring(0, condition.length()).equals(condition)) {
                finalRezault.add(new ConditionProcessor(i.substring(condition.length(), i.indexOf("!%")), i.substring(i.indexOf("!%") + 2, i.indexOf(endCondition)), values, conditions, iterations));
                continue;
            }
            if (i.length() >= repeat.length() && i.substring(0, repeat.length()).equals(repeat)) {
                finalRezault.add(new RepeatProcessor(i.substring(repeat.length(), i.indexOf("!%")), i.substring(i.indexOf("!%") + 2, i.indexOf(endRepeat)), values, conditions, iterations));
                continue;
            }
            finalRezault.add(new ValueProcessor(i.substring(1, i.length() - 1), values, conditions, iterations));

        }

        return this.toString(finalRezault);
    }


}
